// Author: Vinh Dat Hoang
public class BikeStore {
    public static void main(String args[]){
        Bicycle bicycle[] = new Bicycle[4];
        bicycle[0] = new Bicycle("Honda", 4, 30.3);
        bicycle[1] = new Bicycle("USA", 8, 300.3);
        bicycle[2] = new Bicycle("Ford", 6, 200.33);
        bicycle[3] = new Bicycle("Kia", 5, 40.3);

        for(int i = 0 ; i < bicycle.length; i++){
            System.out.println(bicycle[i]);
        }
    }
}
